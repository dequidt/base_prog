---
layout:     support
title:      "Basic Test about Linux and Vim"
categories: teaching
tag:        TP
level:      3A
public:     ima·2a
tool:
    - terminal
    - bash
toc:        true
---

> This objective of this test is to know if you master the most useful
> commands for Linux. Feel free to ask for help if you can't solve some of the
> questions.

## File management

1. Create a folder `test` in your working directory
1. Create a empty file named `test_file.txt` in the folder `~/test`. What is the
   meaning of `~` in `~/test` ?
1. What is the command to change your working directory to
   `/home/imaEns/jdequidt/test` with an absolute path ? Same question with a
   relative path.
1. How many files are in the folder `/home/imaEns/jdequidt/test` ? How many
   directories ? How many executable files ?
1. Can you read the files inside this folder ?
1. Can you modify the files inside the folder ? When it is possible, prove it by
   adding some text (with your name) at the end of the file.
1. What is the command to check if files in a folder are readable / writable /
   executable ?
1. Assuming that your working directory is `/home/imaEns/jdequidt/test`, how do
   you copy the file `users` in your folder `~/test` ? Assuming that your
   working directory is `~/test`, how do you copy the file
   `/home/imaEns/jdequidt/test/users` in your working directory ? Choose one way
   or the other to copy the file `users` in your folder `~/test`.
1. Now in `~/test`, how do you display the content of the filer `users` ?
1. How do you display the first ten lines of this file ? The last ten lines ?

## Common commands

1. Assuming that the group member of `SE3` is `1023`, what is the command to
   display only users that belongs to the group `SE3` ?
1. What do you need to add to the previous command to output the result of the
   command in the file `se3.txt` ?
1. Is the file `se3.txt` alphabetically sorted ? What is the command to output
   the content of `se3.txt` alphabetically sorted ?
1. How can you combine the questions _1._ and _3._ to output the sorted `SE3`
   members from the file `users` ?
1. What is the command to know how many users there are in `SE3` ?
1. The command `getent group` lists all the group created in Polytech Lille.
   Using your __login__, how can you filter the result of the previous command
   to display only the groups you belong to. How can you complement the command
   to know how many groups you belong to.
1. Graduate students in _Embedded Systems_ from 2002 belongs to the group
   `ima2002`. How can you display the users of the group `ima2002` ? Are people
    you know in this group ?

## Cleaning things up

1. The test is almost complete. How do you remove the folder `~/test` and all
   its content ? Do it.
1. Congrats, the test is done :)
