export CLICOLOR=1
# Usdeful for Database module
export PGHOST=serveur-etu.polytech-lille.fr

# Automatic completion
if [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
fi

# Useful commands
alias ll='ls -l --color=auto'
alias la='ls -A'
alias cp='cp -iv'
alias mv='mv -iv'
alias less='less -FSRXc'

export GREP_OPTIONS='--color=auto'

# Prompt modification
function colored_prompt()
{
    local CYAN="\[\033[0;36m\]"
    local GRAY="\[\033[0;38m\]"
    local RED="\[\033[1;31m\]"

    export PS1='\n\[\e[37;46m\] \h \[\e[0m\]\[\e[37;41m\] \u \[\e[0m\]\[\e[37;44m\] \W \[\e[0m\] '
}

colored_prompt