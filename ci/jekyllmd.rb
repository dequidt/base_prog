all
exclude_rule 'MD041'    # first line will be front-matter not #
exclude_rule 'MD013'    # line length, will be wrapped by editor
exclude_rule 'MD002'    # because every jekyll md starts with h2
exclude_rule 'MD033'    # in order to have jekyll html functions
exclude_rule 'MD034'    # in order to have bare URLs in fenced code
